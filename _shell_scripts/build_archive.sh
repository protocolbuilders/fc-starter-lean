#!/usr/bin/bash
# Authors: susann@tag.bio, rocio@tag.bio, jesse@tag.bio

ENVIRONMENT_FILE="_shell_scripts/environment.sh"

if [ -e ${ENVIRONMENT_FILE} ]
then
    echo Sourcing environment variables from environment file: ${ENVIRONMENT_FILE}
    source ${ENVIRONMENT_FILE}
else
    echo Sourcing environment from existing environment
fi

echo Tag.bio jar path: ${TAGBIO_JARS}

COMMAND="
java -Xmx${XMX_ARCHIVE} \
    -jar ${TAGBIO_JARS}/fc_csv_server.jar \
    config=config/config.json \
    data_dir=${DATA_DIR} \
    archive=${DATA_DIR}/archive.ser \
    die=true"

echo Tag.bio build archive command: ${COMMAND}

${COMMAND}