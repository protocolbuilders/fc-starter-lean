#!/usr/bin/bash
# Authors: susann@tag.bio, rocio@tag.bio, jesse@tag.bio

ENVIRONMENT_FILE="_shell_scripts/environment.sh"

if [ -e ${ENVIRONMENT_FILE} ]
then
    echo Sourcing environment variables from environment file: ${ENVIRONMENT_FILE}
    source ${ENVIRONMENT_FILE}
else
    echo Sourcing environment from existing environment
fi

# When in the Tag.bio Developer Studio, these environment variables are already set
echo Tag.bio jar path: ${TAGBIO_JARS}
echo Tag.bio R SDK path: ${TAGBIO_R_UTILS}
echo Tag.bio Python SDK path: ${TAGBIO_PY}

COMMAND="
java -Xmx${XMX_SERVER} \
    -jar ${TAGBIO_JARS}/fc_csv_server.jar \
    archive=${DATA_DIR}/archive.ser \
    r_sdk=${TAGBIO_R_UTILS} \
    python_sdk=${TAGBIO_PY} \
    main=main.json \
    run_tests=true"

echo Tag.bio FC server command: ${COMMAND}

${COMMAND}