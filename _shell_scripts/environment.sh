#!/usr/bin/bash
# Authors: susann@tag.bio, rocio@tag.bio, jesse@tag.bio

# When in the Tag.bio Developer Studio, these environment variables are already set
TAGBIO_R_UTILS=~/sdk/tagbio
TAGBIO_PY=~/sdk/tagbiopy

# This is the path to the FC jar folder - when in the Tag.bio Developer Studio, it will already be set
TAGBIO_JARS=~/jarspace

# This is the path to the source data folder, the archive will also be written and sourced here
DATA_DIR=~/dataspace/foo-data/

# This is the amount of memory to give the archive build (e.g. 2G will use 2 gigabytes of memory)
XMX_ARCHIVE=2G

# This is the amount of memory to give the server running from archive (e.g. 2G will use 2 gigabytes of memory)
XMX_SERVER=2G